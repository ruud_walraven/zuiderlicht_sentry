import Vue from 'vue';
import App from './App.vue';
import * as Sentry from '@sentry/vue';
import { version } from './../package.json';

if (process.env.NODE_ENV !== 'development') {
    Sentry.init({
        Vue,
        dsn: process.env.VUE_APP_SENTRY_DSN,
        release: process.env.VUE_APP_SENTRY_PROJECT + '@' + version,
        dist: process.env.VUE_APP_DIST,
        environment: 'development',
    });
}

new Vue({
    components: {App},
    template: '<App/>',
    render: h => h(App)
}).$mount('#app');
