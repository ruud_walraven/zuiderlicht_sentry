const SentryWebpackPlugin = require('@sentry/webpack-plugin');

// Ophalen package version
process.env.VUE_APP_VERSION = require('./package.json').version;
// Revisie in git, zodat we kunnen refereren naar versiebeheer en de juiste sourcemaps selecteren in Sentry
process.env.VUE_APP_DIST = require('child_process').execSync('git rev-parse --short HEAD').toString().trim();

// Voeg de plugin van Sentry toe tijdens productie-builds om source maps te uploaden
const webpackPlugins = JSON.parse(process.env.VUE_APP_SENTRY_UPLOAD_SOURCEMAPS)
    ? [
        new SentryWebpackPlugin({
            // sentry-cli configuration
            authToken: process.env.SENTRY_AUTH_TOKEN,
            org: 'zuiderlicht',
            project: 'test-source-maps',
            include: './dist/',
            stripPrefix: ['dist'],
            ignore: [
                'node_modules',
                'webpack.config.js',
                'public',
                'src',
                'tests',
            ],
            release: 'test-source-maps@' + process.env.VUE_APP_VERSION,
            dist: process.env.VUE_APP_DIST,
        }),
    ]
    : [];

module.exports = {
    pages: {
        index: {
            entry: 'src/main.js',
            template: 'public/index.html',
        }
    },
    devServer: {
        https: true,
    },
    chainWebpack: config => {
        config.resolve.extensions
            .add('.js')
            .add('.json')
            .add('.vue');
    },
    configureWebpack: {
        devtool: 'source-map',
        plugins: webpackPlugins,
    },
};
